<?php

namespace GeneralBundle\Controller;

use ProductsBundle\Entity\Stock;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;

class DefaultController extends Controller
{
    public function indexAction()
    {
        $em = $this->getDoctrine()->getManager();
        $products = $em->getRepository('ProductsBundle:Product')->findBy([], ['id' => 'DESC'], 5);
        
        return $this->render('GeneralBundle:Default:index.html.twig', array(
            'products' => $products,
        ));
    }

    public function getStockPastAction()
    {
        $em = $this->getDoctrine()->getManager();
        $repo = $em->getRepository(Stock::class);

        $q = $repo->createQueryBuilder('s')
            ->where('s.date < :now')
            ->setParameter('now', new \DateTime())
            ->orderBy('s.date', 'DESC')
            ->setMaxResults(5)
            ->getQuery();

        $stocks = $q->getResult();
        
        return $this->render('ProductsBundle:Stock:getPast.html.twig', array(
            'stocks' => $stocks,
        ));
    }
    
    
}
