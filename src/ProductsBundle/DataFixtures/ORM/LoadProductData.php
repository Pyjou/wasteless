<?php

namespace ProductsBundle\DataFixtures\ORM;

use Doctrine\Common\DataFixtures\FixtureInterface;
use Doctrine\Common\Persistence\ObjectManager;
use ProductsBundle\Entity\Product;


class LoadProductData implements FixtureInterface
{
    public function load(ObjectManager $manager)
    {
        $pdt = new Product();
        $pdt->setName("Pomme de terre");
        $pdt->setDescription("La pomme de terre est originaire de la Cordillère des Andes dans le sud-ouest de l'Amérique du Sud où son utilisation remonte à environt 8000 ans. Introduite en Europe vers la fin du XVIème siècle à la suite de la découverte de l'amérique par les conquistadors espagnols. Aujourd'hui elle est cultivé dans plus de 150 pays à travers le monde.");
        $pdt->setTpsLimit(180);
        $pdt->setPath('pdt.jpg');
        $manager->persist($pdt);

        $haricots = new Product();
        $haricots->setName("Haricots");
        $haricots->setDescription("Cette plante, originaire d'Amérique centrale et d'Amérique du Sud (Andes), joue un rôle important dans l'alimentation humaine comme source d'amidon (féculent), de protéines et dans la fixation biologique de l'azote");
        $haricots->setTpsLimit(5);
        $haricots->setPath("haricots.jpg");
        $manager->persist($haricots);

        $jdf = new Product();
        $jdf->setName("Jus de fruits");
        $jdf->setDescription("Le jus de fruits, comme le jus de légumes, est une boisson obtenue à partir de fruits ou de légumes.");
        $jdf->setTpsLimit(7);
        $jdf->setPath("jdf.jpg");
        $manager->persist($jdf);
        
        $salade = new Product();
        $salade->setName("Salade");
        $salade->setDescription("Le mot salade provient de l'italien insalata, « mets salé » (participe passé du latin tardif salare donnant les herba salata, « herbes potagères salées », plat typique dans la Rome antique) et qu'on retrouve dans le provençal salada. Les Romains avaient en effet l'habitude de conserver olives, radis et autres légumes dans la saumure ou de l'huile et du vinaigre salé. En français, le mot est attesté depuis 1335.");
        $salade->setPath("salade.jpg");
        $salade->setTpsLimit(7);
        $manager->persist($salade);

        $soupe = new Product();
        $soupe->setName("Soupe");
        $soupe->setDescription("La soupe, ou potage, est un aliment liquide ou onctueux (exceptionnellement sans part liquide), froid ou chaud, qui est généralement servi au début du repas.");
        $soupe->setTpsLimit(3);
        $soupe->setPath("soupe.jpg");
        $manager->persist($soupe);

        $carotte = new Product();
        $carotte->setName("Carottes");
        $carotte->setDescription("L'ancêtre sauvage de la carotte provient d'Iran, qui reste le centre de la diversité de Daucus carota. On peut distinguer deux variétés de carottes : la carotte de l'Est et la carotte de l'Ouest. Les variétés maraîchères sont cultivées comme des plantes annuelles car ce qui intéresse le jardinier est la racine tubérisée et non la graine. La plupart des variétés cultivées sont de couleur orange. Il existe aussi des carottes fourragères blanches. Des variétés \"anciennes aujourd'hui\" sont aussi de couleur rouge, violette ou jaune.");
        $carotte->setPath("carotte.jpg");
        $carotte->setTpsLimit(90);
        $manager->persist($carotte);

        $poireaux = new Product();
        $poireaux->setName("Poireaux");
        $poireaux->setDescription("Cette espèce est originaire de la région méditerranéenne, probablement du Proche-Orient. Elle est largement cultivée dans toutes les zones tempérées. C'est un légume très anciennement connu. L'empereur romain Néron fut surnommé le « porrophage » car il en consommait de grandes quantités pour s'éclaircir la voix. Cette plante figurait parmi les plantes potagères recommandées dans le capitulaire De Villis au Moyen Âge.");
        $poireaux->setTpsLimit(7);
        $poireaux->setPath("poireaux.jpg");
        $manager->persist($poireaux);

        $choux = new Product();
        $choux->setName("Choux");
        $choux->setDescription("Les choux se consomment généralement cuits de diverses façon : soupes, pot-au-feu, cuits à l'eau, ou comme légumes d'accompagnement. Ses feuilles gaufrées le rendent particulièrement propice à la fabrication de choux farcis entiers (la farce est introduite entre les feuilles).");
        $choux->setTpsLimit(14);
        $choux->setPath("choux.jpg");
        $manager->persist($choux);

        $jambon = new Product();
        $jambon->setName("Jambon");
        $jambon->setDescription("Le jambon est la cuisse entière d'un animal destinée à être préparée crue. Le plus souvent, c'est le porc qui est utilisé, mais il existe aussi des jambons de sanglier, de marcassin, d'ours, de renne ou de volailles comme le poulet ou la dinde. endu entier ou en tranches, le jambon est une des principales spécialités de la charcuterie où ses préparations alimentaires et même gastronomiques sont multiples et variées.");
        $jambon->setTpsLimit(4);
        $jambon->setPath("jambon.jpg");
        $manager->persist($jambon);

        $oeuf = new Product();
        $oeuf->setName("Oeuf");
        $oeuf->setDescription("L’œuf de volaille est un produit agricole servant d'ingrédient entrant dans la composition de nombreux plats, dans de nombreuses cultures gastronomiques du monde.");
        $oeuf->setTpsLimit(21);
        $oeuf->setPath("oeuf.jpg");
        $manager->persist($oeuf);

        $lait = new Product();
        $lait->setName("Lait");
        $lait->setDescription("Le lait est un liquide biologique comestible généralement de couleur blanchâtre produit par les glandes mammaires des mammifères femelles. Riche en lactose, il est la principale source de nutriments pour les jeunes mammifères avant qu'ils puissent digérer d'autres types d'aliments. L'homme utilise le lait produit par certains mammifères domestiques comme un aliment. ");
        $lait->setTpsLimit(4);
        $lait->setPath("lait.jpg");
        $manager->persist($lait);

        $volaille = new Product();
        $volaille->setName("Volaille");
        $volaille->setDescription("Une volaille est un oiseau domestique, appartenant généralement aux gallinacés ou aux palmipèdes, élevé pour sa chair ou ses œufs, soit en basse-cour traditionnelle, soit en élevage industriel.");
        $volaille->setTpsLimit(2);
        $volaille->setPath("volaille.jpg");
        $manager->persist($volaille);

        $boeuf = new Product();
        $boeuf->setName("Boeuf");
        $boeuf->setDescription("La viande bovine est la viande issue des animaux de l'espèce Bos taurus, qu'il s'agisse de vache, taureau, veau, broutard, taurillon, génisse ou bœuf.");
        $boeuf->setTpsLimit(5);
        $boeuf->setPath("boeuf.jpg");
        $manager->persist($boeuf);

        $agneau = new Product();
        $agneau->setName("Agneau");
        $agneau->setDescription("L'agneau désigne également la viande très tendre de l’agneau. On distingue l'agneau de lait ou agnelet non sevré (10 kg) ; le laiton, appelé aussi agneau blanc (25 kg) en raison de la pâleur de la viande ; le broutard (35 kg), qui est un agneau à chair rosée, qui a connu les pâturages ; et enfin l'agneau de pré-salé, ou estran, qui est un broutard dont la viande à une saveur particulière et une chair plus foncée.");
        $agneau->setTpsLimit(5);
        $agneau->setPath("agneau.jpg");
        $manager->persist($agneau);

        $poisson = new Product();
        $poisson->setName("Poisson");
        $poisson->setDescription("Les poissons sont des animaux vertébrés aquatiques à branchies, pourvus de nageoires et dont le corps est le plus souvent couvert d'écailles.");
        $poisson->setTpsLimit(2);
        $poisson->setPath("poisson.jpg");
        $manager->persist($poisson);

        $fromage = new Product();
        $fromage->setName("Fromage");
        $fromage->setDescription("Le fromage est un aliment obtenu à partir de lait coagulé ou de produits laitiers, comme la crème, puis d'un égouttage suivi ou non de fermentation et éventuellement d'affinage (fromages affinés). Le fromage est fabriqué à partir de lait de vache principalement, mais aussi de brebis, de chèvre, de bufflonne ou d'autres mammifères. Le lait est acidifié, généralement à l'aide d'une culture bactérienne. Une enzyme, la présure, ou un substitut comme de l'acide acétique ou du vinaigre, est ensuite adjointe afin de provoquer la coagulation et former le lait caillé et le petit-lait. Certains fromages comportent de la moisissure, sur la croûte externe et/ou à l'intérieur, et même des larves vivantes comestibles dans certaines régions.");
        $fromage->setTpsLimit(30);
        $fromage->setPath("fromage.jpg");
        $manager->persist($fromage);
        
        $porc = new Product();
        $porc->setName("Porc");
        $porc->setDescription("Interdite dans les religions juive et musulmane, la viande de porc est parmi les viandes les plus consommées au monde. Elle présente un certain nombre de dangers sanitaires (vers, toxines) si, et seulement si elle n'est pas préparée convenablement (la viande de porc doit pour cette raison être soit cuite, soit tranchée très fine). Presque toutes les parties du porc sont utilisables en cuisine, ce qui se traduit par le dicton populaire « Tout est bon dans le cochon », expression attribuée à Brillat-Savarin.");
        $porc->setTpsLimit(4);
        $porc->setPath("porc.jpg");
        $manager->persist($porc);

        $vH = new Product();
        $vH->setName("Viandes hachées");
        $vH->setDescription("Viandes cuites ou crue qui sont hachées.");
        $vH->setTpsLimit(2);
        $vH->setPath("vh.jpg");
        $manager->persist($vH);

        $vC = new Product();
        $vC->setName("Viandes cuites");
        $vC->setDescription("Viandes cuites.");
        $vC->setTpsLimit(4);
        $vC->setPath("vc.jpg");
        $manager->persist($vC);

        $bacon = new Product();
        $bacon->setName("Bacon");
        $bacon->setDescription("Le lard est le terme générique qui désigne trois parties du porc. Celles-ci se trouvent à hauteur du dos, de la poitrine et du ventre du porc. Le lard du dos, appelé « bardière », « panne », est essentiellement fait de graisse ; celui de la poitrine et du ventre contient davantage de viande.");
        $bacon->setTpsLimit(7);
        $bacon->setPath("bacon.jpg");
        $manager->persist($bacon);

        $charcuterie = new Product();
        $charcuterie->setName("Charcuterie");
        $charcuterie->setDescription("Le terme charcuterie désigne couramment de nombreuses préparations alimentaires à base de viande et d'abats, crues ou cuites. Elles proviennent majoritairement, mais pas exclusivement, du porc, dont presque tous les éléments peuvent être utilisés, et ont souvent le sel comme agent de conservation (salaison, saumure).");
        $charcuterie->setTpsLimit(6);
        $charcuterie->setPath("charcuterie.jpg");
        $manager->persist($charcuterie);

        $confiture = new Product();
        $confiture->setName("Confiture");
        $confiture->setDescription("La confiture est une confiserie obtenue, le plus souvent, en faisant cuire dans une bassine à confiture certains fruits, éventuellement dénoyautés et coupés en morceaux, avec un poids équivalent de sucre. C'est une technique de conservation des aliments pour les fruits les plus fragiles, c'est également un moyen de consommer certains fruits astringents comme le coing ou amers comme la bigarade.");
        $confiture->setTpsLimit(30);
        $confiture->setPath("confiture.jpg");
        $manager->persist($confiture);

        $ketchup = new Product();
        $ketchup->setName("Ketchup");
        $ketchup->setDescription("Le ketchup est un condiment populaire, habituellement élaboré à partir de sauce tomate, de vinaigre et de sucre.");
        $ketchup->setTpsLimit(365);
        $ketchup->setPath("ketchup.jpg");
        $manager->persist($ketchup);

        $mayo = new Product();
        $mayo->setName("Mayonnaise");
        $mayo->setDescription("La mayonnaise est une sauce froide à base d'huile émulsionnée dans un mélange de jaune d'œuf et de vinaigre ou jus de citron.");
        $mayo->setTpsLimit(60);
        $mayo->setPath("mayo.jpg");
        $manager->persist($mayo);

        $cm = new Product();
        $cm->setName("Café moulu");
        $cm->setDescription("Le café est une boisson obtenue à partir des graines du caféier, un arbuste du genre Coffea.");
        $cm->setTpsLimit(60);
        $cm->setPath("cm.jpg");
        $manager->persist($cm);

        $beurre = new Product();
        $beurre->setName("Beurre");
        $beurre->setDescription("Le beurre est un aliment constitué par la matière grasse du lait seulement travaillée mécaniquement pour améliorer son goût, sa conservation et diversifier ses utilisations, que ce soit nature, notamment en tartine ou comme corps gras de cuisson des aliments, ou ingrédient de préparations culinaires et notamment pâtissières. Il est conditionné pour être utilisé sous la forme d'une pâte onctueuse, mais sa consistance sensible à la température passe rapidement de l'état solide du réfrigérateur à l'état huileux qu'il prend dès le début de toute cuisson. Du sel y est ajouté ordinairement pour accroître sa conservation ou par respect des usages régionaux.");
        $beurre->setTpsLimit(21);
        $beurre->setPath("beurre.jpg");
        $manager->persist($beurre);

        $fr = new Product();
        $fr->setName("Fruits rouges");
        $fr->setDescription("Les fruits rouges sont un groupement de plusieurs fruits comme les baies, les drupes (cerise, mûre, ...) et les faux-fruits (fraise).");
        $fr->setTpsLimit(2);
        $fr->setPath("fr.jpg");
        $manager->persist($fr);

        $melon = new Product();
        $melon->setName("Melon");
        $melon->setDescription("Le Melon (Cucumis melo) est une plante herbacée annuelle originaire d'Afrique intertropicale, appartenant à la famille des Cucurbitacées et largement cultivée comme plante potagère pour son faux-fruit comestible. Le terme désigne aussi le fruit climactérique lui-même très savoureux, sucré et parfumé.");
        $melon->setTpsLimit(4);
        $melon->setPath("melon.jpg");
        $manager->persist($melon);

        $raisin = new Product();
        $raisin->setName("Raisin");
        $raisin->setDescription("Le raisin sert surtout à la fabrication du vin à partir de son jus fermenté (on parle dans ce cas de raisin de cuve), mais il se consomme également comme fruit, soit frais, le raisin de table, soit sec, le raisin sec qui est utilisé surtout en pâtisserie ou en cuisine. On consomme également du jus de raisin. Des baies, on extrait aussi l'huile de pépins de raisin.");
        $raisin->setTpsLimit(5);
        $raisin->setPath("raisin.jpg");
        $manager->persist($raisin);

        $peche = new Product();
        $peche->setName("Pêche");
        $peche->setDescription("Les pêches sont des fruits climactériques charnus, juteux et sucrés, avec une chair jaune, blanche, ou rouge (sanguine), une peau veloutée de couleur jaune ou orange plus ou moins lavée de rose-carmin à rose-saumon ou brune chez les sanguines, et un noyau dur, adhérent ou non.");
        $peche->setTpsLimit(7);
        $peche->setPath("peche.jpg");
        $manager->persist($peche);

        $pomme = new Product();
        $pomme->setName("Pomme");
        $pomme->setDescription("a pomme est un fruit comestible à pépins d'un goût sucré ou acidulé selon les variétés. D'un point de vue botanique il s'agit d'un faux-fruit. Elle est produite par le pommier, arbre du genre Malus. C'est le fruit le plus consommé en France et le troisième dans le monde");
        $pomme->setTpsLimit(60);
        $pomme->setPath("pomme.jpg");
        $manager->persist($pomme);

        $endive = new Product();
        $endive->setName("Endive");
        $endive->setDescription("Dans beaucoup de recettes, on conseille d’atténuer l’amertume de l'endive par un évidement conique à sa base ou par l’adjonction de sucre. Ce conseil fait bondir les puristes qui assurent que c’est précisément cette amertume qui fait tout l’intérêt de l'endive. Quant au sucre, il ne peut en aucun cas corriger l’amertume ou l’acidité d’un mets puisque ses récepteurs gustatifs sont spécifiques et disjoints de ceux de l'amertume ou de l'acidité.");
        $endive->setTpsLimit(3);
        $endive->setPath("endive.jpg");
        $manager->persist($endive);

        $poivron = new Product();
        $poivron->setName("Poivron");
        $poivron->setDescription("Le poivron (ou piment au Québec) est un groupe de cultivars de l'espèce Capsicum annuum (tout comme certains piments). Ce sont les variétés douces issues de cette espèce par sélection. Ces cultivars doux produisent des fruits de différentes couleurs dont le rouge, le jaune et l'orange. Le fruit est également consommé sous sa forme verte immature. C'est une plante annuelle de la famille des Solanacées originaire du Mexique, d'Amérique centrale et d'Amérique du Sud. La plante est cultivée comme plante potagère pour ses fruits consommés, crus ou cuits, comme légumes. Le terme désigne à la fois le fruit et la plante.");
        $poivron->setTpsLimit(7);
        $poivron->setPath("poivron.jpg");
        $manager->persist($poivron);

        $pain = new Product();
        $pain->setName("Pain");
        $pain->setDescription("Le pain est l'aliment de base traditionnel de nombreuses cultures. Il est fabriqué à partir des ingrédients qui sont la farine et l'eau. Il contient généralement du sel. D'autres ingrédients s'ajoutent selon le type de pain et la manière dont est préparée le pain culturellement. Lorsqu'on ajoute le levain ou la levure, la pâte du pain est soumise à un gonflement dû à la fermentation.");
        $pain->setTpsLimit(7);
        $pain->setPath("pain.jpg");
        $manager->persist($pain);

        $yaourt = new Product();
        $yaourt->setName("Yaourt");
        $yaourt->setDescription("Le yaourt, yahourt, yogourt ou yoghourt, est un lait fermenté par le développement des seules bactéries lactiques thermophiles Lactobacillus delbrueckii subsp. bulgaricus et Streptococcus thermophilus qui doivent être ensemencées simultanément et se trouver vivantes dans le produit fini. C'est la définition officielle française depuis 1963 précisée par le décret de 1988. D'un pays à l'autre, les législations peuvent cependant différer.");
        $yaourt->setTpsLimit(21);
        $yaourt->setPath("yaourt.jpg");
        $manager->persist($yaourt);

        $sandwich = new Product();
        $sandwich->setName("Sandwich");
        $sandwich->setDescription("Le sandwich est un mets généralement composé de deux ou plusieurs tranches de pain avec un ou plusieurs ingrédients entre elles.");
        $sandwich->setTpsLimit(2);
        $sandwich->setPath("sandwich.jpg");
        $manager->persist($sandwich);

        $patisserie = new Product();
        $patisserie->setName("Patisserie");
        $patisserie->setDescription("La pâtisserie désigne à la fois certaines préparations culinaires sucrées à base de pâte, l'art de leur confection, la boutique où se vendent ces préparations faites par un pâtissier, ainsi que l'industrie de préparation et de vente de ces produits : gâteaux et tartes notamment.");
        $patisserie->setTpsLimit(4);
        $patisserie->setPath("patisserie.jpg");
        $manager->persist($patisserie);

        $cf = new Product();
        $cf->setName("Crême fraîche");
        $cf->setDescription("La crème est la matière grasse du lait obtenue par écrémage. La crème aigre, crème fraîche liquide, crème fraîche épaisse... ainsi que le beurre, sont des produits laitiers issus de cette crème.");
        $cf->setTpsLimit(5);
        $cf->setPath("cf.jpg");
        $manager->persist($cf);

        $fromage = new Product();
        $fromage->setName("Fromage");
        $fromage->setDescription("Le fromage est un aliment obtenu à partir de lait coagulé ou de produits laitiers, comme la crème, puis d'un égouttage suivi ou non de fermentation et éventuellement d'affinage (fromages affinés).");
        $fromage->setTpsLimit(21);
        $fromage->setPath("fromage.jpg");
        $manager->persist($fromage);

        $manager->flush();
    }

    public function getOrder()
    {
        return 0;
    }
}