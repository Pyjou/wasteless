<?php

namespace ProductsBundle\Controller;

use Doctrine\ORM\EntityRepository;
use ProductsBundle\Entity\Product;
use ProductsBundle\Entity\Stock;
use Symfony\Bridge\Doctrine\Form\Type\EntityType;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Template;
use Symfony\Component\Form\Extension\Core\Type\DateType;
use Symfony\Component\HttpFoundation\Request;

class StockController extends Controller
{
    /**
     * @Template()
     * @Route("/")
     */
    public function indexAction()
    {
        $em = $this->getDoctrine()->getManager();
        $stock = $em->getRepository(Stock::class)->findByUser($this->getUser());

        return array('stocks' => $stock);
    }

    /**
     * @Template()
     * @Route("/add")
     */
    public function addStockNoProductAction(Request $request)
    {
        $stock = new Stock();
        
        $form = $this->createFormBuilder($stock)
            ->add('product', EntityType::class, [
                'class' => 'ProductsBundle\Entity\Product',
                'choice_label' => 'name'
            ])
            ->add('date', DateType::class, [
                'input' => 'datetime',
                'html5' => true,
                'widget' => 'single_text',
                'format' => 'dd/MM/yyyy'
            ])
            ->getForm();

        $form->handleRequest($request);

        if ($form->isValid()) {
            $em = $this->getDoctrine()->getManager();

            $stock->setUser($this->getUser());

            $em->persist($stock);
            $em->flush();

            return $this->redirectToRoute('products_stock_index');
        }

        return array('form' => $form->createView());
    }

    /**
     * @Template()
     * @Route("/addProduct/{id}")
     */
    public function addStockProductAction(Request $request, $id)
    {
        $em = $this->getDoctrine()->getManager();

        $product = $em->getRepository(Product::class)->findOneBy(['id' => $id]);

        if (!$product) {
            return $this->createNotFoundException('Produit non trouvé !');
        }

        $stock = new Stock();

        $form = $this->createFormBuilder($stock)
            ->add('date', DateType::class, [
                'input' => 'datetime',
                'html5' => true,
                'widget' => 'single_text',
                'format' => 'dd/MM/yyyy'
            ])
            ->getForm();

        $form->handleRequest($request);

        if ($form->isValid()) {
            $em = $this->getDoctrine()->getManager();

            $stock->setProduct($product);
            $stock->setUser($this->getUser());

            $em->persist($stock);
            $em->flush();

            return $this->redirectToRoute('products_stock_index');
        }

        return array(
            'form' => $form->createView(),
            'product' => $product,
        );
    }

    /**
     * @Template()
     * @Route("/remove/{id}")
     */
    public function removeStockAction($id)
    {
        $em = $this->getDoctrine()->getManager();
        $stock = $em->getRepository(Stock::class)->findOneBy(['id' => $id]);

        if ($stock->getUser() != $this->getUser()) {
            return $this->createAccessDeniedException('Ceci n\'est pas dans votre stock !');
        }

        $em->remove($stock);
        $em->flush();
        
        return $this->redirectToRoute('products_stock_index');
    }
}