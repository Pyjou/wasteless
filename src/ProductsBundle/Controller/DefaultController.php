<?php

namespace ProductsBundle\Controller;

use ProductsBundle\Entity\Product;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Template;
use Symfony\Component\HttpFoundation\Request;

class DefaultController extends Controller
{
    /**
     * @Template()
     * @Route("/")
     */
    public function indexAction()
    {
        $em = $this->getDoctrine()->getManager();
        $products = $em->getRepository(Product::class)->findAll();

        return array('products' => $products);
    }

    /**
     * @Template()
     * @Route("/show/{id}")
     */
    public function showAction($id)
    {
        $em = $this->getDoctrine()->getManager();
        $product = $em->getRepository(Product::class)->findOneBy(['id'=>$id]);

        if (!$product) {
            return $this->createNotFoundException("Produit non trouvé");
        }

        return array(
            'product' => $product,
        );
    }

    /**
     * @Template()
     * @Route("/create");
     */
    public function createAction(Request $request)
    {
        $product = new Product();
        $form = $this->createFormBuilder($product)
            ->add('name')
            ->add('file')
            ->add('description')
            ->add('tpsLimit')
            ->getForm();

        $form->handleRequest($request);

        if ($form->isValid()) {
            $em = $this->getDoctrine()->getManager();

            $em->persist($product);
            $em->flush();

            return $this->redirectToRoute('general_homepage');
        }

        return array('form' => $form->createView());
    }

    /**
     * @Template()
     * @Route("/edit/{id}")
     */
    public function editAction(Request $request, $id)
    {
        $em = $this->getDoctrine()->getManager();
        $product = $em->getRepository(Product::class)->findOneBy(['id' => $id]);

        if (!$product) {
            return $this->createNotFoundException("Produit non trouvé !");
        }
        
        $form = $this->createFormBuilder($product)
            ->add('name')
            ->add('file')
            ->add('description')
            ->add('tpsLimit')
            ->getForm();
        
        $form->handleRequest($request);
        
        if ($form->isValid()) {
            $em->persist($product);
            $em->flush();

            return $this->redirectToRoute('products_default_show', ['id' => $id]);
        }

        return array('form' => $form->createView(), 'product' => $product,);
    }
    
    /**
     * @Template()
     * @Route("/delete/{id}")
     */
    public function deleteAction($id)
    {
        $em = $this->getDoctrine()->getManager();
        $product = $em->getRepository(Product::class)->findOneBy(['id' => $id]);

        if (!$product) {
            return $this->createNotFoundException("Produit non trouvé !");
        }

        $em->remove($product);
        $em->flush();

        return $this->redirectToRoute('products_default_index');
    }
}
